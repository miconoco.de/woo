


     ##      ##  .####.    .####. 
     ##.    .## .######.  .######.
      #: ## :#  ###  ###  ###  ###
     :#:.##.:#: ##.  .##  ##.  .##
      # :##:##  ##    ##  ##    ##
      ## ## ##  ##.  .##  ##.  .##
      ###::##   ###  ###  ###  ###
      :##..##:  .######.  .######.
      .##  ##    .####.    .####. 


He who has a 'why' to live for  
can bear almost any 'how'.  
[Nietsche]


About
=====

[woo](https://get.miconoco.de/woo) is modular tool
to ease your daily product work.


Install
=======

    curl https://get.miconoco.de/woo/woo -o ~/.local/bin/woo
    chmod +x ~/.local/bin/woo


Usage
=====

Boot an existing workspace
--------------------------

    mkdir -p <PRODUCT_WORKSPACE>; cd <PRODUCT_WORKSPACE>
    woo boot git@my-scm:path/to/woo/powered/product.git
    . ./activate


Create a new workspace
----------------------

    cd <PRODUCT_WORKSPACE>
    woo boot .
    git remote add origin <WOO-POWERED-PRODUCT.git>
    git commit -a -m 'hello woo master'
    
    git clone <sub-project-a>
    echo /sub-project-a >> .gitignore
    
    git clone <sub-project-b>
    echo /sub-project-b >> .gitignore
    
    git clone <sub-project-c>
    echo /sub-project-c >> .gitignore
    echo /sub-project-c >> .wooignore
    
    woo snapshot
    git commit -a -m 'initialize projects'
    git push -u origin master


Pull changes
------------

    cd <PRODUCT_WORKSPACE>
    . ./activate
    woo pull
    woo snapshot


Changelog
---------

    cd <PRODUCT_WORKSPACE>
    . ./activate
    woo log


Custom woo-command
------------------

    cd <PRODUCT_WORKSPACE>
    . ./activate
    cat <<EOF > ./bin/yesterday
    #!/bin/sh
    git --no-pager log --oneline --since='2 days ago'
    EOF
    chmod + ./bin/yesterday
    git commit -a -m 'hello woo yesterday'
    woo yesterday


Contribution welcome
====================

Grab the [source](https://git.sr.ht/~miconoco/woo)/[mirror](https://codeberg.org/miconoco.de/woo);
get your [hands dirty](TODO) and [send patches](https://git-send-email.io)
or questions to [posaune@miconoco.de](https://lists.sr.ht/~miconoco/posaune).

Similar Tools
=============

- [Colorful KISS helper for git workspaces](https://github.com/StreakyCobra/gws)


License
=======

Simplified BSD, see [LICENSE](LICENSE) for details.

--  
Yours truly,  
[Wizard Of toOls](https://get.miconoco.de/woo)

