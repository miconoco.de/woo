SMU := .smu/smu

## build
build: dist/woo dist/CACHEDIR.TAG dist/CHANGELOG

help: Makefile
	@sed -n 's/^##//p' $<

# source, target, WOO_LOOP_PRE, WOO_LOOP_POST
define add-bin
	@printf '\n' >> $(2).help
	@sed -n -e '/^#!/,/^$$/{{//!p}}' bin/$(1) | sed 's/^#//g' >> $(2).help
	@printf '\n_$(1)() {\n' >> $(2)
	@printf 'WOO_LOOP_PRE=$${WOO_LOOP_PRE:=$(3)}; WOO_LOOP_POST=$${WOO_LOOP_POST:=$(4)}\n' >> $(2)
	@printf 'cat <<"EOF_$(1)" | _exec_stdin "$$@"\n' >> $(2)
	@cat bin/$(1) >> $(2)
	@printf '\nEOF_$(1)\n' >> $(2)
	@printf '\n_bel\n}\n' >> $(2)
endef

define add-cmd
	@printf '\n' >> $(2).help
	@sed -n -e '/^#!/,/^$$/{{//!p}}' cmd/$(1) | sed 's/^#//g' >> $(2).help
	@sed -n -e '2,$$p' cmd/$(1) >> $@
endef

dist/woo: bin/woo LICENSE cmd/activate cmd/boot bin/clone bin/log bin/pull bin/push .git/refs/heads/master Makefile
	@echo mk $@
	@mkdir -p $(@D)
	@printf '' > $(@).help
	@# header
	@printf '#!/bin/sh\n' > $@
	@printf '# https://get.miconoco.de/woo/\n' >> $@
	@git log --format='# %h - %cs%n#' -1 >> $@
	@sed 's/^/# /g' LICENSE >> $@
	@sed -n -e '/^# https/,/^case "$${1:/{//!p}' $< >> $@
	$(call add-cmd,activate,$@)
	$(call add-cmd,boot,$@)
	$(call add-bin,clone,$@,1,0)
	$(call add-bin,log,$@,0,0)
	$(call add-bin,pull,$@,1,0)
	$(call add-bin,push,$@,0,1)
	@# case
	@printf '\n' >> $@
	@sed -n -e '/^case "$${1:/,$$p' $< >> $@
	@sed 's/\(list|snapshot|reset\)/\1|log|clone|boot|pull|push|activate/' -i $@
	@sed -e '/^BUILT-INS$$/r $(@).help' -i $@
	@chmod a+x $@

dist/CHANGELOG: .git/refs/heads/master
	@printf '# Changelog %s\n' "$(git describe --abbrev=0 --tags HEAD 2>/dev/null)" > $@
	@WOOWS=$(PWD) ./bin/log >> $@

dist/woo.sha256: dist/woo
	@sha256sum $< | sed 's/dist\///' > $@

dist/woo.sha256.sig: dist/woo.sha256
	@rm -f $@
	@( cd $(@D) || exit 1; gpg --detach-sig --sign --armor --output $(@F) $(<F); )

sign: dist/woo.sha256.sig

## deploy
deploy: build docs dist/woo.sha256.sig
	@rsync -a -v TODO LICENSE docs/ dist/ \
		--exclude='*.help' \
		--exclude='style.css' \
		--exclude=CACHEDIR.TAG \
		mico:www/get.miconoco.de/woo/

%/CACHEDIR.TAG:
	@mkdir -p $(@D)
	@printf 'Signature: 8a477f597d28d172789f06886806bc55\n' > $@


## docs
docs: docs/header.html docs/footer.html docs/CACHEDIR.TAG docs/style.css .git/refs/heads/master
	@echo mk $@

docs/style.css:
	@mkdir -p $(@D)
	@curl -s https://get.miconoco.de/style.css -o $@

define HEADER
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
  <title>WOO - Your Wizard Of ToOls</title>
  <link rel="stylesheet" href="/style.css">
</head>
<body>
<article id=content>
endef

define FOOTER
<div id=footer class="center lower">
<hr/>
<!-- VERSION -->
</div>
</article>
</body>
</html>
endef

docs/header.html: export HEADER:=$(HEADER)
docs/header.html: README.md $(SMU)
	@mkdir -p $(@D)
	@printf "%s\n" "$${HEADER}" > $@
	@$(SMU) $< >> $@
	@printf "<div id=releases class=center><h1>Releases</h1>" >> $@

docs/footer.html: export FOOTER:=$(FOOTER)
docs/footer.html: dist/woo
	@mkdir -p $(@D)
	@printf "</div>\n%s\n" "$${FOOTER}" \
		| sed 's/<!-- VERSION -->/$(shell git log -1 --pretty=format:"%h - %cs")/' \
		> $@

.smu/smu: .smu/CACHEDIR.TAG
	@curl -s -L https://github.com/karlb/smu/tarball/master \
		| tar xzf - --strip-components=1 -C $(@D)
	@$(MAKE) -s -C $(@D)

## clean
clean:
	@rm -rf -- dist docs

## distclean
distclean: clean
	@rm -rf -- .smu

.PHONY: build clean deploy distclean help sign
